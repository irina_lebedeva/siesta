#!/bin/bash

mkdir work
cd work

. ../../set_siesta_dir.sh "$1" $2

echo "Running script with TranSIESTA=$SIESTA"
if [ -z "$TBT" ] ; then
    TBT="${EXEC_PREFIX} ${ABS_EXEC_DIR}/tbtrans"
fi
echo "Running script with tbtrans=$TBT"

for ELEC in ts_n_terminal_elec_x ts_n_terminal_elec_z
do

    # Start with the electrode calculation
    echo "==> Electrode Calculation for $ELEC"
    mkdir Elec_$ELEC
    cd Elec_$ELEC
    ln ../../$ELEC.fdf .
    $SIESTA --electrode $ELEC.fdf > $ELEC.out
    RETVAL=$?
    if [ $RETVAL -ne 0 ]; then
	echo "The electrode calculation did not go well ..."
	exit
    fi
    cat $ELEC.out >> ../ts_n_terminal.out
    # Go back to base directory
    cd ..

done

for SCAT in ts_n_terminal_3
do
    echo "==> Scattering Region Calculation for $SCAT"
    mkdir Scat_$SCAT
    cd Scat_$SCAT
    ln ../../$SCAT.fdf .
    # Copy the electrode's .TSHS
    for ELEC in ts_n_terminal_elec_x ts_n_terminal_elec_z
    do
	ln ../../$ELEC.TSHS .
    done
    $SIESTA $SCAT.fdf > $SCAT.out
    RETVAL=$?
    if [ $RETVAL -ne 0 ]; then
	echo "** The scattering region calculation for $SCAT did not go well ..."
	exit
    fi
    cat $SCAT.out >> ../ts_n_terminal.out
    rm *.TSGF*

    # Go back to base directory
    cd ..


    # TBTrans calculation
    echo "==> TBTrans Calculation for $SCAT"
    echo "==> Running $SCAT with tbtrans=$TBT"
    mkdir TBT_$SCAT
    cd TBT_$SCAT
    # Copy input files
    for ELEC in ts_n_terminal_elec_x ts_n_terminal_elec_z
    do
	ln ../../$ELEC.TSHS .
    done
    ln ../../$SCAT.TSHS .
    ln ../../$SCAT.fdf .
    $TBT $SCAT.fdf > ${SCAT}_tbt.out
    RETVAL=$?
    if [ $RETVAL -ne 0 ]; then
    	echo "The scattering region calculation did not go well ..."
	    exit
    fi

    cat ${SCAT}_tbt.out >> ../ts_n_terminal.out
    cd ..
done
